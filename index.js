// Ex1:
var numArray = []
  , arrayFloat = [];
function getEle(inputNum) {
    return document.getElementById(inputNum)
}
function getNumber() {
    var inputNum = Number(getEle("inputNum").value);
    numArray.push(inputNum),
    getEle("txtArray").innerHTML = numArray
}
function sumPositive() {
    for (var inputNum = 0, sum = 0; sum < numArray.length; sum++)
        numArray[sum] > 0 && (inputNum += numArray[sum]);
        getEle("txtSum").innerHTML = "Tổng số dương: " + inputNum 
}
// Ex2:
function countPositive() {
    for (var inputNum = 0, sum = 0; sum < numArray.length; sum++)
        numArray[sum] > 0 && inputNum++;
    getEle("txtCount").innerHTML = "Số dương: " + inputNum
}
// Ex3:
function findMin() {
    for (var inputNum = numArray[0], sum = 1; sum < numArray.length; sum++)
        numArray[sum] < inputNum && (inputNum = numArray[sum]);
    getEle("txtMin").innerHTML = "Số nhỏ nhất: " + inputNum
}
// Ex4:
function findMinPos() {
    for (var inputNum
         = [], sum = 0; sum < numArray.length; sum++)
        numArray[sum] > 0 && inputNum.push(numArray[sum]);
    if (inputNum.length > 0) {
        for (var smallestpositivenumber = inputNum[0], sum = 1; sum < inputNum.length; sum++)
            inputNum[sum] < smallestpositivenumber && (smallestpositivenumber = inputNum[sum]);
        getEle("txtMinPos").innerHTML = "Số dương nhỏ nhất: " + smallestpositivenumber
    } else
        getEle("txtMinPos").innerHTML = "Không có số dương trong mảng"
}
// Ex5:
function findEven() {
    for (var inputNum = 0, sum = 0; sum < numArray.length; sum++)
        numArray[sum] % 2 == 0 && (inputNum = numArray[sum]);
    getEle("txtEven").innerHTML = "Số chẵn cuối cùng: " + inputNum
}
// Ex6:
function swap(inputNum, sum) {
    var smallestpositivenumber = numArray[inputNum];
    numArray[inputNum] = numArray[sum],
    numArray[sum] = smallestpositivenumber
}
function changePosition() {
    swap(getEle("inputIndex1").value, getEle("inputIndex2").value),
    getEle("txtChangePos").innerHTML = "Mảng sau khi đổi: " + numArray
}
// Ex7:
function sortIncrease() {
    for (var ipputNum
         = 0; inputNum < numArray.length; inputNum++)
        for (var sum = 0; sum < numArray.length - 1; sum++)
            numArray[sum] > numArray[sum + 1] && swap(sum, sum + 1);
    getEle("txtIncrease").innerHTML = "Mảng sau khi sắp xếp: " + numArray
}
// Ex8:
function checkPrime(inputNum) {
    if (inputNum < 2)
        return !1;
    for (var sum = 2; sum <= Math.sqrt(inputNum); sum++)
        if (inputNum % sum == 0)
            return !1;
    return !0
}
function findPrime() {
    for (var inputNum = -1, sum = 0; sum < numArray.length; sum++) {
        if (checkPrime(numArray[sum])) {
            inputNum = numArray[sum];
            break
        }
    }
    getEle("txtPrime").innerHTML = inputNum
}
// Ex9:
function getFloat() {
    var inputNum = Number(getEle("inputFloat").value);
    arrayFloat.push(inputNum),
    getEle("txtArrayFloat").innerHTML = arrayFloat
}
function findInt() {
    for (var inputNum = 0, sum = 0; sum < arrayFloat.length; sum++)
        Number.isInteger(arrayFloat[sum]) && inputNum++;
    getEle("txtInt").innerHTML = "Số nguyên: " + inputNum
}
// Ex10:
function compareNum() {
    for (var inputNum = 0, sum = 0, smallestpositivenumber = 0; smallestpositivenumber < numArray.length; smallestpositivenumber++)
        numArray[smallestpositivenumber] > 0 ? inputNum++ : numArray[smallestpositivenumber] < 0 && sum++;
    getEle("txtCompare").innerHTML = inputNum > sum ? "Số dương > Số âm" : inputNum < sum ? "Số âm > Số dương" : "Số âm = Số dương"
}
document.addEventListener("contextmenu", function(inputNum) {
    inputNum.preventDefault()
}, !1)